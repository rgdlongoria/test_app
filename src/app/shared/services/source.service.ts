import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { LoremIpsum } from 'lorem-ipsum';
import { DataInterface } from '@shared/models/data.interface';

const MAX_ID = 1084;

const lorem = new LoremIpsum({
  sentencesPerParagraph: {
    max: 8,
    min: 4
  },
  wordsPerSentence: {
    max: 16,
    min: 4
  }
});

@Injectable({
  providedIn: 'root'
})
export class SourceService {

  dataCount = 0;

  private dataChange$ = new Subject<any>();

  constructor() {}

  getData(filter?: string): Observable<DataInterface[]> {

    let dataArray = [...Array.from(Array(4000)).keys()].map((i: number) => {

      const dataId = i + 1;

      return {
        id: dataId,
        photo: environment.image_url + `${dataId % MAX_ID}/500/500`,
        text: this.randomWords(10, 3)
      };
    });

    if (filter) {

      dataArray = dataArray.filter((element: DataInterface) => {

        return (
          (element.id.toString() === filter.trim()) ||
          (element.text.includes(filter.trim()))
        );
      });
    }

    this.dataCount = dataArray.length;

    this.dataChange$.next();

    return of(dataArray);
  }

  public getDataChangeObservable(): Observable<any> {
    return this.dataChange$.asObservable();
  }

  private randomWords(max: number, min: number): string {

    return lorem.generateWords(Math.round(Math.random() * (max - min) + min));
  }
}
