import { TestBed } from '@angular/core/testing';

import { SourceService } from './source.service';

describe('SourceService', () => {
  let service: SourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getData should return value',
    (done: DoneFn) => {
    service.getData().subscribe(value => {
      expect(value.length).toBe(4000);
      done();
    });
  });

  it('getData should return value with filters',
    (done: DoneFn) => {
    service.getData('1').subscribe(value => {
      expect(value.length).toBe(1);
      done();
    });
  });
});
