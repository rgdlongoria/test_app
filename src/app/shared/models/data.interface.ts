export interface DataInterface {
    id: number;
    photo: string;
    text: string;
}
