import { FormsModule } from '@angular/forms';
import { SourceService } from '@shared/services/source.service';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;
  let sourceService: SourceService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePage ],
      imports: [IonicModule.forRoot(), FormsModule],
      providers: [
        SourceService
        // { provide: SourceService, useValue: MockSourceService }
      ],
    }).compileComponents();

    sourceService = TestBed.inject(SourceService);

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('applyFilter should call loadData', () => {

    const spyOnLoadData = spyOn(component, 'loadData').and.callThrough();

    component.filter = '1';

    component.applyFilter();

    expect(spyOnLoadData).toHaveBeenCalled();
  });

  it('startDebounceFilterProgress should increase debounceFilterProgress variable', fakeAsync( () => {

    expect(component.debounceFilterProgress).toEqual(0);

    component.startDebounceFilterProgress(null);

    tick(4000);

    clearInterval(component.debounceFilterProgresdId);

    expect(component.debounceFilterProgress).toBeGreaterThan(0);
  }) );
});
