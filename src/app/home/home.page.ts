import { DataInterface } from '@shared/models/data.interface';
import { SourceService } from '@shared/services/source.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {

  listOfElements: DataInterface[];
  filter: string;
  debounceFilterProgress = 0;
  numOfElements = 0;
  debounceFilterTime = 2200;
  debounceFilterProgresdId: any;
  literals = {
    filterPlaceholder: 'Escribe para filtrar por id y texto'
  };

  private destroy$: Subject<boolean> = new Subject<boolean>();
  private dataChangeSubscription: Subscription;

  constructor(
    private sourceService: SourceService
  ) {

    this.dataChangeSubscription = this.sourceService.getDataChangeObservable()
    .pipe(takeUntil(this.destroy$))
    .subscribe(() => {
      this.numOfElements = this.sourceService.dataCount;
    });
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  loadData(filter?: string): void {
    this.sourceService.getData(filter)
    .pipe(takeUntil(this.destroy$))
    .subscribe((dataArray: DataInterface[]) => {
      this.listOfElements = dataArray;
    });
  }

  startDebounceFilterProgress($event: any) {

    this.resetFilterProgress();

    this.debounceFilterProgresdId = setInterval(() => {
      this.debounceFilterProgress = this.debounceFilterProgress + 0.03125;
    }, 62);
  }

  applyFilter() {

    this.resetFilterProgress();

    this.loadData(this.filter);
  }

  private resetFilterProgress() {

    clearInterval(this.debounceFilterProgresdId);
    this.debounceFilterProgress = 0;
  }
}
